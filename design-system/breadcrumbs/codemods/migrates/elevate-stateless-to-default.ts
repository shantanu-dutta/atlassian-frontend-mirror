import { elevateComponentToDefault } from '../utils';

export const elevateStatelessToDefault = elevateComponentToDefault(
  '@atlaskit/breadcrumbs',
  'BreadcrumbsStateless',
);
