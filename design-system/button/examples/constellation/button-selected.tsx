import React from 'react';

import Button from '../../src';

export default () => <Button isSelected>Selected button</Button>;
