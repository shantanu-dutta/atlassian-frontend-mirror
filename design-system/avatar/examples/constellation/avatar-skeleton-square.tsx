import React from 'react';

import { Skeleton } from '../../src';

export default function AvatarSkeletonSquareExample() {
  return <Skeleton appearance="square" />;
}
