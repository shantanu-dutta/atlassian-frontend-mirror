import React from 'react';

import { N0 } from '@atlaskit/theme/colors';

import { AtlassianLogo } from '../../src';

export default () => <AtlassianLogo textColor={N0} iconColor={N0} />;
